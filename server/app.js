// The goal of this server is just to trim a video and provide a download
// For the sake of simplicity and due to it's small size I will include it right next to the main
// client app and both will run together

const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');

ffmpeg.setFfmpegPath(ffmpegPath);

// Set up the express app
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// Use cors and fileUpload
app.use(cors());
app.use(fileUpload());
app.use('/public', express.static(__dirname + '/public'));

// endpoint to test connection
app.route('/api').get((req, res) => {
  res.status(200).send({
    success: 'true',
    message: 'This is working as expected'
  })
});

// endpoint to upload files
app.route('/upload').post((req, res) => {
  let imageFile = req.files.file;
  imageFile.mv(`${__dirname}/public/originals/${req.body.filename}`, function(err) {
    if (err) {
      return res.status(500).send(err);
    }
    res.json({file: `public/originals/${req.body.filename}`});
  });
});

// endpoint to process the video, trim it, and provide the file to the client
app.route('/download').get((req, res) => {
  const { originalName, from, to, clipName } = req.query;
  const duration = to - from;
  const fileExtension = originalName.split('.')[1];
  ffmpeg(`${__dirname}/public/originals/${originalName}`)
    .setStartTime(from)
    .setDuration(duration)
    .output(`${__dirname}/public/clips/${clipName}.${fileExtension}`)
    .on('end', function(err) {
      if(!err)
      {
        console.log('conversion Done');
        const file = `${__dirname}/public/clips/${clipName}.${fileExtension}`;
        res.download(file);
      }
    })
    .on('error', function(err){
      console.log(err);
    }).run();
});

const PORT = 8000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});
