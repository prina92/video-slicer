import axios from 'axios';
import { store } from "../index";
import { setLoading } from "../redux/actions";

export function handleUploadImage(file, fileName, successCallback, errorCallback) {
  store.dispatch(setLoading(true));
  const data = new FormData();
  data.append('file', file);
  data.append('filename', fileName);

  axios.post('http://localhost:8000/upload', data)
  .then(res => {
    store.dispatch(setLoading(false));
    successCallback();
    console.log(res);
  })
  .catch(err => {
    store.dispatch(setLoading(false));
    console.log(err);
    errorCallback();
  });
}
