import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import '../../styles/App.css';
import Header from "../../components/Header/Header";
import MainPage from "../MainPage/MainPage";
import Notification from "../../components/Notification/Notification";

class App extends React.Component {
  render() {
    const { notification } = this.props;
    return (
      <div className="App">
        {notification &&
          // Only display notifications once these are stored in redux's store
          <Notification
            alertClass={notification.class}
            alertTitle={notification.title}
            alertText={notification.text}
          />
        }
        <Header/>
        <MainPage/>
      </div>
    );
  }
}

App.propTypes = {
  notification: PropTypes.object,
};

// inject into props selected part of the redux's store
function mapStateToProps(state) {
  return {
    notification: state.get("notification"),
  };
}

export default connect(mapStateToProps)(App);
