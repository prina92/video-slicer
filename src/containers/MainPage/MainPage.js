import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
import {
  Col,
  Grid,
  Row
} from 'react-bootstrap';
import VideoPlayer from "../../components/VideoPlayer/VideoPlayer";
import StartPage from "../../components/StartPage/StartPage";
import VideoList from "../../components/VideoList/VideoList";
import ClipCreator from "../../components/ClipCreator/ClipCreator";
import CustomModal from "../../components/CustomModal/CustomModal";
import Info from "../../components/Info/Info";
import {
  deleteClipError, deleteClipSuccess, deleteModalContent,
  videoUploadedSuccess, videoUploadError
} from "../../messages/messages";
import { setNotification } from "../../redux/actions";
import { handleUploadImage } from "../../service/imageService";

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      from: null,
      to: null,
      videoFile: null,
      videoList: [],
      selectedVideo: 0,
      currentVideoName: null,
      showDeleteModal: false,
      indexToDelete: null,
      editIndex: null,
      editVideo: null,
    };
  }

  handleFileChange = (e) => {
    // store selected file into state and display notification
    e.preventDefault();
    const file = this.uploadInput.files[0];
    const auxVideoList = this.state.videoList;
    auxVideoList.push({
      name: file.name,
      slice: 'Total',
      tag: 'Original'
    });
    handleUploadImage(file, file.name, () => {
      this.setState({ videoFile: file, videoList: auxVideoList, currentVideoName: file.name });
      this.props.actions.setNotification({
        class: 'success',
        title: videoUploadedSuccess,
      });
    }, () => {
      this.props.actions.setNotification({
        class: 'danger',
        title: videoUploadError,
      });
    });
  };

  selectVideo = (index) => {
    // Check if index exists within videoList and set video data into state to be shown
    if (index < this.state.videoList.length) {
      const nextVideo = this.state.videoList[index];
      this.setState({
        from: nextVideo.from,
        to: nextVideo.to,
        selectedVideo: index,
        currentVideoName: nextVideo.name,
      });
    }
  };

  editClip = (videoIndex) => {
    // receive index of the video to edit and store it in state to be passed to ClipCreator component
    this.setState({
      editIndex: videoIndex,
      editVideo: this.state.videoList[videoIndex]
    });
    // Select original video in order to show it and display ClipCreator component
    this.selectVideo(0);
  };

  addClip = (video) => {
    // Push new video into VideoList array and select it so it's shown right after creation
    const auxVideoListState = this.state.videoList;
    auxVideoListState.push(video);
    const index = auxVideoListState.length - 1;
    this.setState({
      videoList: auxVideoListState,
    });
    this.selectVideo(index);
  };

  updateClip = (video, index) => {
    // Store in given position of the VideoList array the updated video
    const auxVideoListState = [...this.state.videoList];
    auxVideoListState[index] = video;
    this.setState({
      videoList: auxVideoListState,
      editVideo: null,
      editIndex: null,
    });
    this.selectVideo(index);
  };

  cancelUpdate = () => {
    this.setState({
      editVideo: null,
      editIndex: null,
    });
  };

  showDeleteModal = (videoIndex) => {
    // Receive index of video to be deleted and show modal
    this.setState({
      showDeleteModal: true,
      indexToDelete: videoIndex,
    });
  };

  closeDeleteModal = () => {
    // reset index to delete and hide modal
    this.setState({
      showDeleteModal: false,
      indexToDelete: null,
    });
  };

  deleteClip = () => {
    // remove given index from the VideoList array and display notification
    const index = this.state.indexToDelete;
    if (index > 0) {
      const auxVideoList = this.state.videoList;
      auxVideoList.splice(index, 1);
      this.setState({ videoList: auxVideoList });
      this.selectVideo(0); // after deleting select original video
      this.props.actions.setNotification({
        class: 'success',
        title: deleteClipSuccess,
      });
    } else {
      // This should never happen as the code is right now, but I'd rather have a notification anyway
      this.props.actions.setNotification({
        class: 'danger',
        title: deleteClipError.title,
        text: deleteClipError.content,
      });
    }
    this.closeDeleteModal();
  };

  render() {
    const {
      currentVideoName, from, to, videoList, videoFile, selectedVideo,
      editIndex, showDeleteModal, editVideo
    } = this.state;
    const videoUrl = videoFile ? URL.createObjectURL(videoFile) : null;
    return (
      <Grid style={{ paddingRight: 15, paddingLeft: 15, width: '100%' }}>
        {/*Ghost input to  upload a file. Won't display*/}
        <input
          ref={(ref) => {this.uploadInput = ref;}} type="file"
          accept="video/*" style={{display: 'none'}}
          onChange={this.handleFileChange}
        />
        <Row>
          {videoFile ?
            // Show this controls only if the user already selected a video file
            <div>
              <Col md={7}>
                <VideoPlayer
                  key={from}  video={videoUrl} autoPlay muted from={from} to={to}
                  videoName={currentVideoName} currentVideoIndex={selectedVideo}
                  isLastVideo={selectedVideo === videoList.length - 1}
                  selectVideo={this.selectVideo}
                />
                {selectedVideo === 0 ?
                  // Only show ClipCreator controls for original video, located in first position of array
                  // The key prop is to get a brand new re render of the component when it changes
                  <ClipCreator
                    key={`${this.props.videoDuration}${editIndex}`}
                    videoDuration={this.props.videoDuration}
                    addClip={this.addClip} setNotification={this.props.actions.setNotification}
                    updateClip={this.updateClip} editIndex={editIndex} editVideo={editVideo}
                    cancelUpdate={this.cancelUpdate}
                  /> :
                  // Otherwise show info
                  <Info createClip={() => this.selectVideo(0)} />
                }
              </Col>
              <Col md={5}>
                <VideoList
                  videoList={videoList}  handleClickRow={this.selectVideo}
                  selectedVideo={selectedVideo} editClip={this.editClip}
                  deleteVideo={this.showDeleteModal}
                  setNotification={this.props.actions.setNotification}
                />
              </Col>
            </div> :
            // If no video file show start component with intro and button to upload
            <StartPage
              handleUploadClick={() => this.uploadInput.click()}
              loading={this.props.loading}
            />
          }
        </Row>
        <CustomModal
          show={showDeleteModal}
          handleClose={this.closeDeleteModal}
          heading="Delete clip"
          subTitle={deleteModalContent.title}
          text={deleteModalContent.text}
          closeLabel="Cancel"
          acceptClass="danger"
          handleAccept={this.deleteClip}
          acceptLabel="Delete"
        />
      </Grid>
    )
  }
}

MainPage.propTypes = {
  videoDuration: PropTypes.number,
  actions: PropTypes.object,
  loading: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    videoDuration: state.get("videoDuration"),
    loading: state.get("loading"),
  };
}

function mapDispatchToProps(dispatch) {
  const actions = {
    setNotification: bindActionCreators(setNotification, dispatch),
  };
  return { actions };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
