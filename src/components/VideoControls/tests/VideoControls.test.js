import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import VideoControls from '../VideoControls';

enzyme.configure({ adapter: new Adapter() });

describe('<VideoControls />', () => {
  const mockedProps = {
    clickPlay: jest.fn(),
    currentTime: '00:10',
    duration: 35,
    from: 10,
    nextVideo: jest.fn(),
    nextVideoDisabled: true,
    percentage: 50,
    playIcon: 'play',
    prevVideo: jest.fn(),
    prevVideoDisabled: false,
    setCurrentTime: jest.fn(),
    timeDuration: '00:45',
    to: 45,
    toggleVolume: jest.fn(),
    videoName: 'mockedName',
    videoMuted: false,
  };
  let renderedComponent;
  renderedComponent = enzyme.shallow(
    <VideoControls {...mockedProps} />
  );
  it('Render matches snapshot', () => {
    expect(renderedComponent).toMatchSnapshot();
  });
  it('handleOnClick calls setCurrentTime with given values', () => {
    renderedComponent.instance().progressBarRef = {
      current: {
        getBoundingClientRect: jest.fn(),
      }
    };
    let ref = renderedComponent.instance().progressBarRef;
    ref.current.getBoundingClientRect.mockReturnValue({
      left: 20,
      width: 400,
    });
    const mockedClickEvent = { pageX: 150 };
    renderedComponent.instance().handleOnClick(mockedClickEvent);
    expect(mockedProps.setCurrentTime).toHaveBeenCalledTimes(1);
    expect(mockedProps.setCurrentTime).toHaveBeenCalledWith(21.55);
  });
  it('clicking the progress bar triggers function', () => {
    const progressBar = renderedComponent.find('div.custom-progress-bar');
    renderedComponent.instance().handleOnClick = jest.fn();
    progressBar.simulate('click', 'mockedEvent');
    expect(renderedComponent.instance().handleOnClick).toHaveBeenCalledTimes(1);
    expect(renderedComponent.instance().handleOnClick).toHaveBeenCalledWith('mockedEvent');
  })
});
