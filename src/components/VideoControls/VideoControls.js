import React from 'react';
import { ButtonGroup, Button, Glyphicon } from 'react-bootstrap';
import PropTypes from 'prop-types';
import '../../styles/customVideoControls.css'

class VideoControls extends React.Component {
  constructor(props) {
    super(props);
    this.progressBarRef = React.createRef();
  }

  handleOnClick = (e) => {
    const { duration, from, to, setCurrentTime } = this.props;
    // get X position where the user clicked within the progress bar
    const bounds = this.progressBarRef.current.getBoundingClientRect();
    const widthClicked = e.pageX - bounds.left;
    const totalWidth = bounds.width;
    // calculate the % within the bar was clicked, with no decimals
    const calc = widthClicked / totalWidth * 100;
    const calcFixed = calc.toFixed(0);
    // calculate duration depending in ranges (if no prop 'to' was provided that means it's the original video)
    const durationCalc = to ? to - from : duration - from;
    // set video time based on this % calculated (add prop 'from' if it's a clip created, otherwise it defaults to 0)
    setCurrentTime((calcFixed * durationCalc / 100) + from);
  };

  render() {
    const {
      percentage, prevVideo, nextVideo, prevVideoDisabled, nextVideoDisabled,
      playIcon, videoMuted, videoName, timeDuration, clickPlay, toggleVolume,
      currentTime
    } = this.props;
    return (
      <div>
        <div
          className="custom-progress-bar"
          ref={this.progressBarRef}
          onClick={(e) => { this.handleOnClick(e); }}
        >
          <div
            className="custom-progress-bar-fill progress-bar-danger"
            // width of the progress bar adjusts to calculated percentage
            style={{
              width: `${percentage}%`,
            }}
          />
        </div>
        <ButtonGroup>
          <Button
            onClick={prevVideo}
            disabled={prevVideoDisabled}
          >
            <Glyphicon glyph="step-backward" />
          </Button>
          <Button onClick={clickPlay}>
            <Glyphicon glyph={playIcon} />
          </Button>
          <Button
            onClick={nextVideo}
            disabled={nextVideoDisabled}
          >
            <Glyphicon glyph="step-forward" />
          </Button>
          <Button
            onClick={toggleVolume}
          >
            <Glyphicon glyph={videoMuted ? 'volume-off' : 'volume-up'} />
          </Button>
        </ButtonGroup>
        <span style={{ verticalAlign: 'middle', marginLeft: 10 }}>
          {`${currentTime}/${timeDuration}`}
        </span>
        <span className="video-title text-right">{videoName}</span>
      </div>
    );
  }
}

VideoControls.propTypes = {
  clickPlay: PropTypes.func,
  currentTime: PropTypes.string,
  duration: PropTypes.number,
  from: PropTypes.number,
  nextVideo: PropTypes.func,
  nextVideoDisabled: PropTypes.bool,
  percentage: PropTypes.number,
  playIcon: PropTypes.string,
  prevVideo: PropTypes.func,
  prevVideoDisabled: PropTypes.bool,
  setCurrentTime: PropTypes.func,
  timeDuration: PropTypes.string,
  to: PropTypes.number,
  toggleVolume:PropTypes.func,
  videoMuted: PropTypes.bool,
  videoName: PropTypes.string,
};

export default VideoControls;
