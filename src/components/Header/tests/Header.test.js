import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Header from '../Header';

enzyme.configure({ adapter: new Adapter() });

describe('<Header />', () => {
  const renderedComponent = enzyme.shallow(
    <Header />
  );
  it('Render matches snapshot', () => {
    expect(renderedComponent).toMatchSnapshot();
  });
});
