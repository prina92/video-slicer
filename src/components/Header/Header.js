import React from 'react';
import logo from '../../assets/logo.svg';
import '../../styles/App.css';

function Header() {
  return (
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">React Video Slicer</h1>
      </header>
  );
}

export default Header;
