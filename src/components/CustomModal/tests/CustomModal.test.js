import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CustomModal from '../CustomModal';

enzyme.configure({ adapter: new Adapter() });

describe('<CustomModal />', () => {
  const mockedProps = {
    show: true,
    handleClose: jest.fn(),
    heading: 'mockedHeading',
    subTitle: 'mockedSubtitle',
    text: 'mockedText',
    closeLabel: 'mockedCloseLabel',
    acceptClass: 'success',
    handleAccept: jest.fn(),
    acceptLabel: 'mockedAcceptLabel',
  };
  const renderedComponent = enzyme.shallow(
    <CustomModal { ...mockedProps } />
  );
  it('Render matches snapshot', () => {
    expect(renderedComponent).toMatchSnapshot();
  });
});
