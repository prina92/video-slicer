import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'react-bootstrap';

function CustomModal({ ...props }) {
  const {
    show, handleClose, heading, subTitle, closeLabel,
    acceptClass, handleAccept, acceptLabel, text,
  } = props;
  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{heading}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <h4>{subTitle}</h4>
        <p>{text}</p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={handleClose}>{closeLabel}</Button>
        <Button bsStyle={acceptClass} onClick={handleAccept}>{acceptLabel}</Button>
      </Modal.Footer>
    </Modal>
  );
}

CustomModal.propTypes = {
  show: PropTypes.bool,
  handleClose: PropTypes.func,
  heading: PropTypes.string,
  subTitle: PropTypes.string,
  text: PropTypes.string,
  closeLabel: PropTypes.string,
  acceptClass: PropTypes.string,
  handleAccept: PropTypes.func,
  acceptLabel: PropTypes.string,
};

export default CustomModal;
