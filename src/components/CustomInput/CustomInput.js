import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

function CustomInput({ ...props }) {
  const { formId, label, inputType, inputValue, inputPlaceholder, handleOnChange, fieldError } = props;
  return (
    <FormGroup
      validationState={fieldError ? 'error' : null}
      controlId={formId}
    >
      <ControlLabel>{label}</ControlLabel>
      <FormControl
        type={inputType}
        value={inputValue}
        placeholder={inputPlaceholder}
        onChange={(e) => handleOnChange(e.target.value)}
      />
    </FormGroup>
  );
}

CustomInput.propTypes = {
  formId: PropTypes.string,
  label: PropTypes.string,
  inputType: PropTypes.string,
  inputValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  inputPlaceholder: PropTypes.string,
  handleOnChange: PropTypes.func,
  fieldError: PropTypes.bool,
};

export default CustomInput;
