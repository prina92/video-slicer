import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CustomInput from '../CustomInput';

enzyme.configure({ adapter: new Adapter() });

describe('<CustomInput />', () => {
  const mockedProps = {
    formId: 'mockedId',
    label: 'Mocked Label',
    inputType: 'text',
    inputValue: 'someString',
    inputPlaceholder: 'mockedPlaceholder',
    handleOnChange: jest.fn(),
  };
  describe('without error field', () => {
    const renderedComponent = enzyme.shallow(
      <CustomInput { ...mockedProps } fieldError={null} />
    );
    it('Render matches snapshot', () => {
      expect(renderedComponent).toMatchSnapshot();
    });
    it('FormControl child calls handleOnchange on onChange event', () => {
      const formControl = renderedComponent.find('FormControl');
      const mockedEvent = { target: {value: 'mockedString' } };
      formControl.prop('onChange')(mockedEvent);
      expect(mockedProps.handleOnChange).toHaveBeenCalledTimes(1);
      expect(mockedProps.handleOnChange).toHaveBeenCalledWith(mockedEvent.target.value);
    });
  });
  describe('with error field', () => {
    const renderedComponent = enzyme.shallow(
      <CustomInput { ...mockedProps } fieldError={true} />
    );
    it('Render matches snapshot', () => {
      expect(renderedComponent).toMatchSnapshot();
    });
  });
});
