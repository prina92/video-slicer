import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import { Col, Button } from 'react-bootstrap';
import 'rc-slider/assets/index.css';
import CustomInput from '../CustomInput/CustomInput';

const Range = Slider.Range;

function RangeSelector({ ...props }) {
  const {
    lowerBound, upperBound, range, handleApply, videoDuration,
    setLowerBound, setUpperBound, handleSliderChange, errorFields,
  } = props;
  return (
    <div>
      <Col md={5}>
        <CustomInput
          fieldError={errorFields.lowerBound}
          formId="from"
          label="Start of clip"
          inputType="text"
          inputPlaceholder="Time of start"
          inputValue={lowerBound}
          handleOnChange={(value) => setLowerBound(value)}
        />
      </Col>
      <Col md={5}>
        <CustomInput
          fieldError={errorFields.upperBound}
          formId="to"
          label="End of clip"
          inputType="text"
          inputPlaceholder="Time of end"
          inputValue={upperBound}
          handleOnChange={(value) => setUpperBound(value)}
        />
      </Col>
      <Col md={2}>
        <div className="text-right" style={{ marginTop: 25 }}>
          <Button
            bsStyle="primary"
            onClick={handleApply}
          >
            Apply
          </Button>
        </div>
      </Col>
      <Col md={12}>
        <Range
          allowCross={false}
          value={range}
          onChange={handleSliderChange}
          min={0} max={videoDuration}
        />
      </Col>
    </div>
  );
}

RangeSelector.propTypes = {
  videoDuration: PropTypes.number,
  lowerBound: PropTypes.string,
  upperBound: PropTypes.string,
  range: PropTypes.array,
  setLowerBound: PropTypes.func,
  setUpperBound: PropTypes.func,
  handleSliderChange: PropTypes.func,
  handleApply: PropTypes.func,
  errorFields: PropTypes.object,
};

export default RangeSelector;
