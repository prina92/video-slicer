import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ClipCreator from '../ClipCreator';
import * as stringUtils from "../../../utils/stringUtils";
import * as validationUtils from "../../../utils/validationUtils";
import {
  clipNameContainsSlash, clipNameErrorNullOrEmpty, createClipSuccess, rangeErrorBounds, rangeErrorFormat,
  rangeErrors, updateClipSuccess
} from "../../../messages/messages";

enzyme.configure({ adapter: new Adapter() });

describe('<ClipCreator />', () => {
  describe('Clip creator when new clip', () => {
    let renderedComponent;
    let mockedProps = {
      videoDuration: 20,
      editIndex: null,
      editVideo: null,
      addClip: jest.fn(),
      setNotification: jest.fn(),
      updateClip: jest.fn(),
      cancelUpdate: jest.fn(),
    };
    beforeEach(() => {
      renderedComponent = enzyme.shallow(
        <ClipCreator { ...mockedProps }  />
      );
    });
    it('Render matches snapshot', () => {
      expect(renderedComponent).toMatchSnapshot();
    });
    it('Custom input childs set State when trigering onChange', () => {
      const mockedName = 'mockedName';
      const mockedTag = 'mockedTag';
      const firstInput = renderedComponent.find('CustomInput').at(0);
      const secondInput = renderedComponent.find('CustomInput').at(1);
      firstInput.prop('handleOnChange')(mockedName);
      secondInput.prop('handleOnChange')(mockedTag);
      const actualNameState = renderedComponent.instance().state.name;
      const actualTagState = renderedComponent.instance().state.tag;
      expect(actualNameState).toEqual(mockedName);
      expect(actualTagState).toEqual(mockedTag);

    });
    describe('Methods', () => {
      let clipCreatorInstance;
      beforeEach(() => {
        clipCreatorInstance = renderedComponent.instance();
      });
      describe('Clear error', () => {
        it('Sets state with field names and false value', () => {
          const mockedFields = ['field1', 'field2'];
          const expectedState = {
            field1: false,
            field2: false,
          };
          clipCreatorInstance.clearError(mockedFields);
          expect(clipCreatorInstance.state.errorFields).toEqual(expectedState);
        });
      });
      describe('onLowerBoundChange', () => {
        const mockedValue = 'test123';
        it('calls clearError once with fields name', () => {
          const actualFields = ['lowerBound', 'rangeError'];
          clipCreatorInstance.clearError = jest.fn();
          clipCreatorInstance.onLowerBoundChange(mockedValue);
          expect(clipCreatorInstance.clearError).toHaveBeenCalledTimes(1);
          expect(clipCreatorInstance.clearError).toHaveBeenCalledWith(actualFields);
        });
        it('sets lowerBound state with given value', () => {
          clipCreatorInstance.onLowerBoundChange(mockedValue);
          expect(clipCreatorInstance.state.lowerBound).toEqual(mockedValue);
        });
      });
      describe('onUpperBoundChange', () => {
        const mockedValue = 'test123';
        it('calls clearError once with fields name', () => {
          const actualFields = ['upperBound', 'rangeError'];
          clipCreatorInstance.clearError = jest.fn();
          clipCreatorInstance.onUpperBoundChange(mockedValue);
          expect(clipCreatorInstance.clearError).toHaveBeenCalledTimes(1);
          expect(clipCreatorInstance.clearError).toHaveBeenCalledWith(actualFields);

        });
        it('sets upperBound state with given value', () => {
          clipCreatorInstance.onUpperBoundChange(mockedValue);
          expect(clipCreatorInstance.state.upperBound).toEqual(mockedValue);
        });
      });
      describe('onSliderChange', () => {
        const mockedRange = [10, 20];
        it('calls clearError once with fields name', () => {
          const actualFields = ['upperBound', 'lowerBound', 'rangeError'];
          clipCreatorInstance.clearError = jest.fn();
          clipCreatorInstance.onSliderChange(mockedRange);
          expect(clipCreatorInstance.clearError).toHaveBeenCalledTimes(1);
          expect(clipCreatorInstance.clearError).toHaveBeenCalledWith(actualFields);
        });
        it('sets range state with given value and lowerBound/upperBound with converted values', () => {
          const formattedLowerBound = '00:10';
          const formattedUpperBound = '00:20';
          clipCreatorInstance.onSliderChange(mockedRange);
          expect(clipCreatorInstance.state.range).toEqual(mockedRange);
          expect(clipCreatorInstance.state.lowerBound).toEqual(formattedLowerBound);
          expect(clipCreatorInstance.state.upperBound).toEqual(formattedUpperBound);
        });
        it('calls toTimeFormat twice with ranges', () => {
          stringUtils.toTimeFormat = jest.fn();
          clipCreatorInstance.onSliderChange(mockedRange);
          expect(stringUtils.toTimeFormat).toHaveBeenCalledTimes(2);
          expect(stringUtils.toTimeFormat).toHaveBeenCalledWith(mockedRange[0]);
          expect(stringUtils.toTimeFormat).toHaveBeenCalledWith(mockedRange[1]);
        })
      });
      describe('handleApply', () => {
        validationUtils.durationInputValidation = jest.fn();
        afterEach(() => {
          jest.resetAllMocks();
        });
        describe('with fieldErrors', () => {
          // force error
          beforeEach(() => {
            validationUtils.durationInputValidation.mockReturnValue(['test']);
          });
          it('calls durationInputValidation once with given values', () => {
            const expectedArg = [
              {
                name: 'lowerBound',
                value: clipCreatorInstance.state.lowerBound,
              },
              {
                name: 'upperBound',
                value: clipCreatorInstance.state.upperBound,
              },
            ];
            clipCreatorInstance.handleApply();
            expect(validationUtils.durationInputValidation).toHaveBeenCalledTimes(1);
            expect(validationUtils.durationInputValidation).toHaveBeenCalledWith(expectedArg);
          });
          it('sets errorFields state', () => {
            const expectedState = {
              test: true,
            };
            clipCreatorInstance.handleApply();
            expect(clipCreatorInstance.state.errorFields).toEqual(expectedState);
          });
          it('calls setNotification once with given values', () => {
            clipCreatorInstance.handleApply();
            const expectedArgs = {
              class: 'danger',
              title: rangeErrorFormat.title,
              text: rangeErrorFormat.content,
            };
            expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
            expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedArgs);
          });
        });
        describe('with bounds error', () => {
          beforeEach(() => {
            validationUtils.durationInputValidation.mockReturnValue([]);
            clipCreatorInstance.setState({
              lowerBound: '00:10',
              upperBound: '00:05',
            });
          });
          it('sets error state with given values', () => {
            clipCreatorInstance.handleApply();
            const expectedState = {
              upperBound: true,
              lowerBound: true,
              rangeError: true,
            };
            expect(clipCreatorInstance.state.errorFields).toEqual(expectedState);
          });
          it('calls setNotification once with given values', () => {
            clipCreatorInstance.handleApply();
            const expectedArgs = {
              class: 'danger',
              title: rangeErrorBounds.title,
              text: rangeErrorBounds.content,
            };
            expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
            expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedArgs);
          });
        });
        describe('without errors', () => {
          beforeEach(() => {
            validationUtils.durationInputValidation.mockReturnValue([]);
            clipCreatorInstance.setState({
              lowerBound: '00:05',
              upperBound: '00:10',
            });
          });
          it('sets the range state with given values', () => {
            clipCreatorInstance.handleApply();
            const expectedState = [5, 10];
            expect(clipCreatorInstance.state.range).toEqual(expectedState);
          });
        });
      });
      describe('checkErrors', () => {
        it('returns true when it finds errors within the state', () => {
          clipCreatorInstance.setState({ errorFields: { test: true } });
          const returnedValue = clipCreatorInstance.checkErrors();
          expect(returnedValue).toBeTruthy();
        });
        it('returns false when it doesnt finds errors within the state', () => {
          clipCreatorInstance.setState({ errorFields: {}});
          const returnedValue = clipCreatorInstance.checkErrors();
          expect(returnedValue).toBeFalsy();
        });
      });
      describe('createClip', () => {
        validationUtils.validateClipName = jest.fn();
        afterEach(() => {
          jest.resetAllMocks();
        });
        describe('with errorName', () => {
          describe('with null name', () => {
            beforeEach(() => {
              validationUtils.validateClipName.mockReturnValue('NULL_OR_EMPTY_ERROR');
              clipCreatorInstance.setState({ name: null });
            });
            it('sets errorFields state with name', () => {
              clipCreatorInstance.createClip();
              const expectedState = { name: true };
              expect(clipCreatorInstance.state.errorFields).toEqual(expectedState);
            });
            it('calls setNotification with expected args', () => {
              clipCreatorInstance.createClip();
              const expectedArgs = {
                class: 'danger',
                title: clipNameErrorNullOrEmpty.title,
                text: clipNameErrorNullOrEmpty.content,
              };
              expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
              expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedArgs);
            });
          });
          describe('with empty string name', () => {
            beforeEach(() => {
              validationUtils.validateClipName.mockReturnValue('NULL_OR_EMPTY_ERROR');
              clipCreatorInstance.setState({ name: '' });
            });
            it('sets errorFields state with name', () => {
              clipCreatorInstance.createClip();
              const expectedState = { name: true };
              expect(clipCreatorInstance.state.errorFields).toEqual(expectedState);
            });
            it('calls setNotification with expected args', () => {
              clipCreatorInstance.createClip();
              const expectedArgs = {
                class: 'danger',
                title: clipNameErrorNullOrEmpty.title,
                text: clipNameErrorNullOrEmpty.content,
              };
              expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
              expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedArgs);
            });
          });
          describe('with slash in string name', () => {
            beforeEach(() => {
              validationUtils.validateClipName.mockReturnValue('SLASH_ERROR');
              clipCreatorInstance.setState({ name: 'test/' });
            });
            it('sets errorFields state with name', () => {
              clipCreatorInstance.createClip();
              const expectedState = { name: true };
              expect(clipCreatorInstance.state.errorFields).toEqual(expectedState);
            });
            it('calls setNotification with expected args', () => {
              clipCreatorInstance.createClip();
              const expectedArgs = {
                class: 'danger',
                title: clipNameContainsSlash.title,
                text: clipNameContainsSlash.content,
              };
              expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
              expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedArgs);
            });
          });
        });
        describe('with checkErrors returning true', () => {
          it('calls setNotification with expected args', () => {
            validationUtils.validateClipName.mockReturnValue(false);
            clipCreatorInstance.checkErrors = jest.fn();
            clipCreatorInstance.checkErrors.mockReturnValue(true);
            clipCreatorInstance.createClip();
            const expectedArgs = {
              class: 'danger',
              title: rangeErrors.title,
              text: rangeErrors.content,
            };
            expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
            expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedArgs);
          });
        });
        describe('with no errors', () => {
          beforeEach(() => {
            validationUtils.validateClipName.mockReturnValue(false);
            clipCreatorInstance.checkErrors = jest.fn();
            clipCreatorInstance.checkErrors.mockReturnValue(false);
          });
          it('it calls addClip and setNotifications with expected args', () => {
            clipCreatorInstance.setState({
              name: 'mockedName',
              tag: 'mockedTag',
              lowerBound: '00:10',
              upperBound: '00:25',
              range: [10, 25],
            });
            const expectedAddClipArgs = {
              name: clipCreatorInstance.state.name,
              tag: clipCreatorInstance.state.tag,
              slice: `${clipCreatorInstance.state.lowerBound} - ${clipCreatorInstance.state.upperBound}`,
              from: clipCreatorInstance.state.range[0],
              to: clipCreatorInstance.state.range[1],
            };
            const expectedSetNotificationArgs = {
              class: 'success',
              title: createClipSuccess ,
            };
            clipCreatorInstance.createClip();
            expect(mockedProps.addClip).toHaveBeenCalledTimes(1);
            expect(mockedProps.addClip).toHaveBeenCalledWith(expectedAddClipArgs);
            expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
            expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedSetNotificationArgs);
          });
        });
      });
    });
  });
  describe('ClipCreator when editing clip', () => {
    let renderedComponent;
    let clipCreatorInstance;
    let mockedProps = {
      videoDuration: 20,
      editIndex: 1,
      editVideo: {
        name: 'test',
        tag: 'mocked',
        from: 2,
        to: 4,
      },
      addClip: jest.fn(),
      setNotification: jest.fn(),
      updateClip: jest.fn(),
      cancelUpdate: jest.fn(),
    };
    beforeEach(() => {
      renderedComponent = enzyme.shallow(
        <ClipCreator { ...mockedProps } />
      );
      clipCreatorInstance = renderedComponent.instance();
    });
    it('Render matches snapshot', () => {
      expect(renderedComponent).toMatchSnapshot();
    });
    describe('createClip function when editing', () => {
      it('it calls addClip and setNotifications with expected args', () => {
        clipCreatorInstance.setState({
          name: 'mockedName',
          tag: 'mockedTag',
          lowerBound: '00:10',
          upperBound: '00:25',
          range: [10, 25],
        });
        const expectedAddClipArgs = {
          name: clipCreatorInstance.state.name,
          tag: clipCreatorInstance.state.tag,
          slice: `${clipCreatorInstance.state.lowerBound} - ${clipCreatorInstance.state.upperBound}`,
          from: clipCreatorInstance.state.range[0],
          to: clipCreatorInstance.state.range[1],
        };
        const expectedSetNotificationArgs = {
          class: 'success',
          title: updateClipSuccess ,
        };
        clipCreatorInstance.createClip();
        expect(mockedProps.updateClip).toHaveBeenCalledTimes(1);
        expect(mockedProps.updateClip).toHaveBeenCalledWith(expectedAddClipArgs, mockedProps.editIndex);
        expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
        expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedSetNotificationArgs);
      });
    });
  });
});
