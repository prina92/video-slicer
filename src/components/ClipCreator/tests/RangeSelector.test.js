import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RangeSelector from '../RangeSelector';

enzyme.configure({ adapter: new Adapter() });

describe('<RangeSelector />', () => {
  const mockedProps = {
    videoDuration: 2,
    lowerBound: '00:10',
    upperBound: '00:25',
    range: [10, 25],
    setLowerBound: jest.fn(),
    setUpperBound: jest.fn(),
    handleSliderChange: jest.fn(),
    handleApply: jest.fn(),
    errorFields: { test: '123' },
  };
  const renderedComponent = enzyme.shallow(
    <RangeSelector { ...mockedProps } />);
  it('Render matches snapshot', () => {
    expect(renderedComponent).toMatchSnapshot();
  });
  it('Child custom inputs calls correct functions onChange', () => {
    const firstInput = renderedComponent.find('CustomInput').at(0);
    const secondInput = renderedComponent.find('CustomInput').at(1);
    firstInput.prop('handleOnChange')('mockedValue');
    secondInput.prop('handleOnChange')('mockedValue2');
    expect(mockedProps.setLowerBound).toHaveBeenCalledTimes(1);
    expect(mockedProps.setUpperBound).toHaveBeenCalledTimes(1);
    expect(mockedProps.setLowerBound).toHaveBeenCalledWith('mockedValue');
    expect(mockedProps.setUpperBound).toHaveBeenCalledWith('mockedValue2');
  });
});
