import React from 'react';
import { Col, Form, Button, Alert, ButtonToolbar } from 'react-bootstrap';
import PropTypes from 'prop-types';
import CustomInput from "../CustomInput/CustomInput";
import RangeSelector from "./RangeSelector";
import { timeStringToSeconds, toTimeFormat } from "../../utils/stringUtils";
import {durationInputValidation, validateClipName} from "../../utils/validationUtils";
import {
  clipNameContainsSlash,
  clipNameErrorNullOrEmpty,
  createClipSuccess, rangeErrorBounds, rangeErrorFormat, rangeErrors,
  updateClipSuccess
} from "../../messages/messages";

class ClipCreator extends  React.Component {
  constructor(props) {
    super(props);
    const { editVideo } = this.props;
    // Initialize state with editVideo fields populated or empty
    this.state = {
      name: editVideo ? editVideo.name : '',
      tag: editVideo ? editVideo.tag : '',
      lowerBound: editVideo ? toTimeFormat(editVideo.from) : toTimeFormat(0),
      upperBound: editVideo ? toTimeFormat(editVideo.to) : toTimeFormat(this.props.videoDuration),
      range: [
        editVideo ? editVideo.from : 0,
        editVideo ? editVideo.to : this.props.videoDuration
      ],
      errorFields: {},
    };
  }

  clearError = (fields) => {
    // we iterate to received fields array and set it's state to false
    const auxErrorState = { ...this.state.errorFields };
    fields.forEach((fieldName) => {
      auxErrorState[fieldName] = false;
    });
    this.setState({ errorFields: auxErrorState });
  };

  onLowerBoundChange = (value) => {
    // clear the error when receiving input from the user
    this.clearError(['lowerBound', 'rangeError']);
    this.setState({ lowerBound: value });
  };

  onUpperBoundChange = (value) => {
    // clear the error when receiving input from the user
    this.clearError(['upperBound', 'rangeError']);
    this.setState({ upperBound: value });
  };

  onSliderChange = (range) => {
    // clear the error when receiving input from the user
    this.clearError(['upperBound', 'lowerBound', 'rangeError']);
    this.setState({
      range,
      lowerBound: toTimeFormat(range[0]),
      upperBound: toTimeFormat(range[1]),
    });
  };

  handleApply = () => {
    const { lowerBound, upperBound, errorFields } = this.state;
    const auxErrorState = { ...errorFields };
    // look for errors in fields
    const fieldErrors = durationInputValidation([
      {
        name: 'lowerBound',
        value: lowerBound
      },
      {
        name: 'upperBound',
        value: upperBound,
      }
    ]);
    // if we receive errors here that means there's a wrong input in the bounds
    if (fieldErrors.length > 0) {
      fieldErrors.forEach((field) => {
        auxErrorState[field] = true;
      });
      // set the error state and dispatch notification
      this.setState({ errorFields: auxErrorState });
      this.props.setNotification({
        class: 'danger',
        title: rangeErrorFormat.title,
        text: rangeErrorFormat.content,
      });
    } else if (lowerBound >= upperBound) {
      // if we enter this if that means the bounds are wrong
      // so we set error state and dispatch a notification
      auxErrorState.upperBound = true;
      auxErrorState.lowerBound = true;
      auxErrorState.rangeError = true;
      this.setState({ errorFields: auxErrorState });
      this.props.setNotification({
        class: 'danger',
        title: rangeErrorBounds.title,
        text: rangeErrorBounds.content,
      });
    }
    else {
      // if everything above went ok then we set the range state
      this.setState({
        range: [
          timeStringToSeconds(lowerBound),
          timeStringToSeconds(upperBound),
        ],
      });
    }
  };

  checkErrors = () => {
    // we iterate over the error state looking for true fields
    const { errorFields } = this.state;
    let error = false;
    Object.keys(errorFields).forEach((key) => {
      if (errorFields[key]) {
        error = true;
      }
    });
    // return false if no errors found
    return error;
  };

  createClip = () => {
    const { name, tag, lowerBound, upperBound, range, errorFields } = this.state;
    // we check if name field is empty or null, and check for other errors
    const errorName = validateClipName(name);
    if (errorName) {
      // if we find errors we set error state and dispatch notification
      let messageToDisplay = clipNameErrorNullOrEmpty;
      if (errorName === 'SLASH_ERROR') { messageToDisplay = clipNameContainsSlash; }
      const auxErrorState = { ...errorFields };
      auxErrorState.name = true;
      this.setState({ errorFields: auxErrorState });
      this.props.setNotification({
        class: 'danger',
        title: messageToDisplay.title,
        text: messageToDisplay.content,
      });
    } else if (this.checkErrors()) {
      // checking range errors
      this.props.setNotification({
        class: 'danger',
        title: rangeErrors.title,
        text: rangeErrors.content,
      });
    } else {
      // if everything above went ok we setup video object to save
      const newVideo = {
        name,
        tag,
        slice: `${lowerBound} - ${upperBound}`,
        from: range[0],
        to: range[1],
      };
      // if we have edit index that means we need to update an existing clip so we use another function
      if (this.props.editIndex) {
        this.props.updateClip(newVideo, this.props.editIndex);
      } else {
        this.props.addClip(newVideo);
      }
      // dispatch success notification after saving
      this.props.setNotification({
        class: 'success',
        title: this.props.editIndex ? updateClipSuccess : createClipSuccess ,
      });
    }
  };

  render() {
    const { lowerBound, upperBound, range, errorFields, name, tag } = this.state;
    return (
      <div style={{ marginTop: 10, marginBottom: 10 }}>
        <Form>
          {this.props.editIndex &&
            // if we are editing we display message to the user so it's not confusing
            <Alert bsStyle="warning">
              You are currently editing <strong>'{this.props.editVideo.name}'</strong>.
            </Alert>
          }
          <Col md={8}>
            <CustomInput
              fieldError={errorFields.name}
              formId="name"
              label="Name"
              inputType="text"
              inputPlaceholder="Enter name for your new clip"
              inputValue={name}
              handleOnChange={(value) => this.setState({ name: value }, this.clearError(['name']))}
            />
          </Col>
          <Col md={4}>
            <CustomInput
              formId="tag"
              label="Tag"
              inputType="text"
              inputPlaceholder="Enter a tag (optional)"
              inputValue={tag}
              handleOnChange={(value) => this.setState({ tag: value })}
            />
          </Col>
          <RangeSelector
            videoDuration={this.props.videoDuration}
            lowerBound={lowerBound}
            upperBound={upperBound}
            range={range}
            setLowerBound={this.onLowerBoundChange}
            setUpperBound={this.onUpperBoundChange}
            handleSliderChange={this.onSliderChange}
            handleApply={this.handleApply}
            errorFields={errorFields}
          />
          <Col md={12}>
            <ButtonToolbar style={{ marginTop: 15, float: 'right' }}>
              {this.props.editIndex &&
              <Button
                bsStyle="warning"
                onClick={this.props.cancelUpdate}
                bsSize="large"
              >
                Cancel edition
              </Button>
              }
              <Button
                bsStyle="success"
                onClick={this.createClip}
                bsSize="large"
              >
                {this.props.editIndex ?
                  // change button label if we are editing a clip
                  'Update clip!' :
                  'Create clip!'}
              </Button>
            </ButtonToolbar>
          </Col>
        </Form>
      </div>
    );
  }
}

ClipCreator.propTypes = {
  videoDuration: PropTypes.number,
  addClip: PropTypes.func,
  setNotification: PropTypes.func,
  updateClip: PropTypes.func,
  editIndex: PropTypes.number,
  editVideo: PropTypes.object,
  cancelUpdate: PropTypes.func,
};

export default ClipCreator;
