import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import MouseTrap from 'mousetrap';
import '../../styles/customVideoControls.css'
import { toTimeFormat } from "../../utils/stringUtils";
import VideoControls from "../VideoControls/VideoControls";
import loading from '../../assets/loading.gif';
import countdown from '../../assets/countdown.gif';
// Actions
import { setVideoDuration } from "../../redux/actions";

class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);
    // initialize state with empty video data
    this.state = {
      videoReady: false,
      videoPaused: true,
      videoCurrentTime: 0,
      videoDuration: 0,
      videoPercentage: 0,
      videoFinished: false,
      videoMuted: false,
    };
  }
  componentDidMount() {
    // after mounting attach event listeners to our video and set the ready state to true
    this.setState({ videoReady: this.videoRef && true });
    this.videoRef.addEventListener('loadedmetadata', this.handleLoadedMetadata);
    this.videoRef.addEventListener('timeupdate', this.handleTimeUpdate);
    this.videoRef.addEventListener('ended', this.handleEndVideo);
    this.videoRef.addEventListener('play', this.handlePlay);
    this.videoRef.currentTime = this.props.from;
    // Binding hotkeys to functions
    MouseTrap.bind('ctrl+alt+n', this.nextVideo);
    MouseTrap.bind('ctrl+alt+p', this.prevVideo);
  }

  componentWillUnmount() {
    // before unmounting pause video and remove event listeners and key bindings
    // this will happen everytime user selects a clip from the list
    this.videoRef.pause();
    this.videoRef.removeEventListener('loadedmetadata', this.handleLoadedMetadata);
    this.videoRef.removeEventListener('timeupdate', this.handleTimeUpdate);
    this.videoRef.removeEventListener('ended', this.handleEndVideo);
    this.videoRef.removeEventListener('play', this.handlePlay);
    MouseTrap.unbind('ctrl+alt+n', this.nextVideo);
    MouseTrap.unbind('ctrl+alt+p', this.prevVideo);
  }

  handlePlay = () => {
    // we kill the time out in case the video ended and was about to autoplay next video
    this.killTimeOut();
    this.setState({
      videoFinished: false,
    });
  };

  killTimeOut = () => {
    // clear time out only if the identifier exists
    if (this.timerIdentifier) {
      clearTimeout(this.timerIdentifier);
    }
  };

  toggleVolume = () => {
    // just inverting muted state
    this.videoRef.muted = !this.videoRef.muted;
    this.setState({ videoMuted: this.videoRef.muted });
  };

  handleEndVideo = () => {
    // when video finished play next video in 3 seconds and set proper state
    const { currentVideoIndex, selectVideo } = this.props;
    this.setState({ videoFinished: true, videoPaused: false });
    this.killTimeOut();
    this.timerIdentifier = setTimeout(() => {
      selectVideo(currentVideoIndex + 1);
    }, 3000);
  };

  handleLoadedMetadata = () => {
    // after video loaded set duration in component's state and redux's store
    this.setState({
      videoDuration: this.videoRef.duration - this.props.from,
    });
    this.props.actions.setVideoDuration(this.videoRef.duration);
  };
  handleTimeUpdate = () => {
    const { to, from } = this.props;
    // every time the video updates its play time we make calculations for the progress bar
    const currentTime = this.videoRef.currentTime;
    // the duration depends on the range the user selected. if prop 'to' was passed, this means it's a clip created by the user
    // otherwise it's the original video. prop 'from' equals 0 if nothing was passed from the parent
    const duration = to ? to - from : this.videoRef.duration - from;
    const percentage = 100 * (currentTime - this.props.from) / duration;
    // percentage >= 100 means video finished. we pause it and call the function that handles this
    if (percentage >= 100) {
      this.videoRef.pause();
      this.handleEndVideo();
    }
    // this state it's being passed constantly to the progress bar
    this.setState({
      videoCurrentTime: currentTime,
      videoPercentage: percentage,
    });
  };
  handlePlayClick = () => {
    // this checks paused state and plays or pauses depending on that
    // state is set to decide which icon display in the button
    this.setState({ videoPaused: this.videoRef.paused });
    if (this.videoRef.paused) {
      if (this.state.videoPercentage >= 100) {
        this.videoRef.currentTime = this.props.from;
      }
      this.videoRef.play();
    } else {
      this.videoRef.pause();
    }
  };

  handleSliderClick = (value) => {
    // set video's time to clicked range and kill time out in case it was about to play next video
    this.videoRef.currentTime = value;
    this.killTimeOut();
    this.setState({ videoFinished: false });
  };

  nextVideo = () => {
    // select next video in the list, except if current video is the last
    const { isLastVideo, currentVideoIndex, selectVideo } = this.props;
    if (!isLastVideo) {
      selectVideo(currentVideoIndex + 1);
    }
  };

  prevVideo = () => {
    // select previous video in the list, except if current video is the first
    const { currentVideoIndex, selectVideo } = this.props;
    if (currentVideoIndex !== 0) {
      selectVideo(currentVideoIndex - 1);
    }
  };

  render() {
    const {
      autoPlay, controls, from, videoName,
      loop, to, video, muted,
      currentVideoIndex, isLastVideo
    } = this.props;
    const {
      videoCurrentTime, videoDuration, videoPaused, videoFinished,
      videoReady, videoMuted, videoPercentage
    } = this.state;
    const isFirstVideo = currentVideoIndex === 0;
    const currentTime = toTimeFormat(videoCurrentTime - from);
    const duration = to ? toTimeFormat(to - from) : toTimeFormat(videoDuration);
    // set play button icon depending on state (playing, paused, finished)
    let playIcon = "play";
    if (videoPaused) {
      playIcon = "pause";
    }
    if (videoFinished) {
      playIcon = "repeat";
    }
    return (
      <div>
        <div className="video-wrapper">
          <video
            poster={loading}
            className="player"
            autoPlay={autoPlay}
            controls={controls}
            loop={loop}
            muted={muted}
            ref={(v) => { this.videoRef = v }}
            src={video}
            height="100%"
            width="100%"
          />
          {videoFinished &&
            <div className="video-overlays">
              {!isLastVideo &&
                // display overlay img only if it's not the last video
                <img src={countdown} className="overlay-img" alt="" />
              }
            </div>
          }
        </div>
        {videoReady &&
          <div>
            <VideoControls
              duration={videoDuration} from={from} percentage={videoPercentage}
              to={to} playIcon={playIcon} videoMuted={videoMuted}
              prevVideo={this.prevVideo}
              nextVideo={this.nextVideo}
              prevVideoDisabled={isFirstVideo} nextVideoDisabled={isLastVideo}
              setCurrentTime={(value) => this.handleSliderClick(value)}
              videoName={videoName} currentTime={currentTime}
              timeDuration={duration} clickPlay={this.handlePlayClick}
              toggleVolume={this.toggleVolume}
            />
          </div>
        }
      </div>

    );
  }
}

VideoPlayer.propTypes = {
  autoPlay: PropTypes.bool,
  controls: PropTypes.bool,
  from: PropTypes.number,
  loop: PropTypes.bool,
  muted: PropTypes.bool,
  actions: PropTypes.object,
  to: PropTypes.number,
  currentVideoIndex: PropTypes.number,
  selectVideo: PropTypes.func,
  video: PropTypes.string,
  isLastVideo: PropTypes.bool,
};
VideoPlayer.defaultProps = {
  from: 0,
};

function mapDispatchToProps(dispatch) {
  const actions = {
    setVideoDuration: bindActionCreators(setVideoDuration, dispatch),
  };
  return { actions };
}

export default connect(null, mapDispatchToProps)(VideoPlayer);
