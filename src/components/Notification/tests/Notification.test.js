import React from 'react';
import configureStore from 'redux-mock-store';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { fromJS } from 'immutable';
import Notification from '../Notification';

enzyme.configure({ adapter: new Adapter() });

describe('<Notification />', () => {
  const mockStore = configureStore();
  const stateStore = fromJS({
    notification: {
      title: 'mockedTitle',
      text: 'mockedText',
    },
  });
  const store = mockStore(stateStore);
  store.dispatch = jest.fn();
  let mockedProps = {
    alertClass: 'success',
    actions: {
      setNotification: jest.fn()
    },
    alertTitle: 'mockedTitle',
    alertText: 'mockedText',
  };

  const renderedComponent = enzyme.shallow(
    <Notification { ...mockedProps } store={store} />
  ).dive();
  it('Render matches snapshot', () => {
    expect(renderedComponent).toMatchSnapshot();
  });
});
