import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';
import { setNotification } from "../../redux/actions";
import '../../styles/App.css';

function Notification({ ...props }) {
  const { alertClass, actions, alertTitle, alertText } = props;
  const handleDismiss = () => {
    actions.setNotification(null);
  };
  return (
    <div className="notification-wrapper">
      <Alert
        bsStyle={alertClass} onDismiss={handleDismiss}
        style={{ float: 'right', maxWidth: 400 }}
        className="notification"
      >
        <h4>{alertTitle}</h4>
        <p>{alertText}</p>
      </Alert>
    </div>
  );
}

Notification.propTypes = {
  alertClass: PropTypes.string,
  actions: PropTypes.object,
  alertTitle: PropTypes.string,
  alertText: PropTypes.string,
};

function mapDispatchToProps(dispatch) {
  const actions = {
    setNotification: bindActionCreators(setNotification, dispatch),
  };
  return { actions };
}

export default connect(null, mapDispatchToProps)(Notification);
