import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import StartPage from '../StartPage';

enzyme.configure({ adapter: new Adapter() });

describe('<StartPage />', () => {
  const mockedProps = {
    handleUploadClick: jest.fn(),
  };
  let renderedComponent;
  describe('with no loading', () => {
    renderedComponent = enzyme.shallow(
      <StartPage
        { ...mockedProps }
        loading={false}
      />
    );
    it('Render matches snapshot', () => {
      expect(renderedComponent).toMatchSnapshot();
    });
  });
  describe('with loading', () => {
    renderedComponent = enzyme.shallow(
      <StartPage
        { ...mockedProps }
        loading={true}
      />
    );
    it('Render matches snapshot', () => {
      expect(renderedComponent).toMatchSnapshot();
    });
  });
});
