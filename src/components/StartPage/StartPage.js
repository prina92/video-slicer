import React from 'react';
import PropTypes from 'prop-types';
import { Button, Jumbotron } from 'react-bootstrap';

function StartPage({ ...props }) {
  return  (
    <Jumbotron style={{ marginLeft: 70, marginRight: 70 }}>
      <h1>Welcome to React Video Slicer!</h1>
      <p>
        With this tool you can upload videos and slice clips for further download.
      </p>
      <p>To get started upload a video!</p>
      <p className="text-right">
        <Button
          bsStyle="primary"
          onClick={props.handleUploadClick}
          disabled={props.loading}
        >
          {props.loading ? 'Uploading video...' : 'Upload video'}
        </Button>
      </p>
    </Jumbotron>
  );
}

StartPage.propTypes = {
  handleUploadClick: PropTypes.func,
  loading: PropTypes.bool,
};

export default StartPage;
