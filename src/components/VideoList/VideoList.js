import React from 'react';
import PropTypes from 'prop-types';
import { Table, Button, ButtonGroup, Glyphicon } from 'react-bootstrap';
import '../../styles/customVideoControls.css';
import CustomInput from "../CustomInput/CustomInput";
import {downloadClicked} from "../../messages/messages";

class VideoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: '',
      originalFile: this.props.videoList[0].name,
    };
  }

  matchesFilter = (tag) => {
    const { filter } = this.state;
    // only check filter if it's not null and it's not empty
    if (filter && filter !== '') {
      return tag.toLowerCase().includes(filter.toLowerCase());
    }
    return true;
  };

  handleClickDownload = () => {
    this.props.setNotification({
      class: 'warning',
      title: downloadClicked.title,
      text: downloadClicked.content,
    });
  };

  drawTable = () => {
    return this.props.videoList.map((video, index) => {
      // check if tag matches filter, otherwise don't draw that row
      if (this.matchesFilter(video.tag)) {
        // Format the names we are passing to the server in the call to avoid conflict
        const encodedNewName = encodeURI(video.name);
        const encodedOriginalName = encodeURI(this.state.originalFile);
        const downloadParams =
          `originalName=${encodedOriginalName}&clipName=${encodedNewName}&from=${video.from}&to=${video.to}`;
        // iterate through videos in the list and build table's rows
        const isActive = this.props.selectedVideo === index;
        const isOriginal = index === 0;
        return (
          <tr
            key={index}
            className={isActive ? 'active-video' : ''}
          >
            <td className="text-left" style={{ verticalAlign: 'middle' }}>{video.name}</td>
            <td className="text-center" style={{ verticalAlign: 'middle' }}>{video.slice}</td>
            <td className="text-center" style={{ verticalAlign: 'middle' }}>{video.tag}</td>
            <td className="text-right" style={{ verticalAlign: 'middle' }}>
              {isOriginal ?
                // if the video is the original show only play button
                <Button
                  bsStyle="success" bsSize="small" disabled={isActive}
                  onClick={() => this.props.handleClickRow(index)}
                  style={{ float: 'right' }}
                >
                  <Glyphicon glyph="play"/>
                </Button> :
                // otherwise show all controls
                <ButtonGroup style={{ float: 'right' }}>
                  <Button
                    bsStyle="success" bsSize="small" disabled={isActive}
                    onClick={() => this.props.handleClickRow(index)}
                  >
                    <Glyphicon glyph="play"/>
                  </Button>
                  <Button
                    bsStyle="warning" bsSize="small"
                    onClick={() => this.props.editClip(index)}
                  >
                    <Glyphicon glyph="edit"/>
                  </Button>
                  <Button
                    bsStyle="danger" bsSize="small"
                    onClick={() => this.props.deleteVideo(index)}
                  >
                    <Glyphicon glyph="trash"/>
                  </Button>
                  <Button
                    onClick={this.handleClickDownload}
                    bsStyle="primary" bsSize="small"
                    href={`http://localhost:8000/download?${downloadParams}`}
                  >
                    <Glyphicon glyph="download"/>
                  </Button>
                </ButtonGroup>
              }
            </td>
          </tr>
        );
      }
    });
  };

  render() {
    return (
      <div>
        <CustomInput
          formId="filterField"
          label="Filter by tag"
          inputType="text"
          inputValue={this.state.filter}
          inputPlaceholder="Filter clips by tag"
          handleOnChange={(value) => this.setState({ filter: value })}
        />
        <Table bordered>
          <thead>
            <tr>
              <th>Name</th>
              <th className="text-center">Slice</th>
              <th className="text-center">Tag</th>
              <th className="text-right">Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.drawTable()}
          </tbody>
        </Table>
      </div>
    );
  }
}

VideoList.propTypes = {
  handleClickRow: PropTypes.func,
  videoList: PropTypes.array,
  selectedVideo: PropTypes.number,
  deleteVideo: PropTypes.func,
  editClip: PropTypes.func,
  setNotification: PropTypes.func,
};

export default VideoList;
