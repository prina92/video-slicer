import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import VideoList from '../VideoList';
import {downloadClicked} from "../../../messages/messages";

enzyme.configure({ adapter: new Adapter() });

describe('<VideoList />', () => {
  const mockedProps = {
    handleClickRow: jest.fn(),
    videoList: [
      {
        name: 'mockedName1',
        tag: 'mockedTag1',
        from: 2,
        to: 15,
        slice: 'mockedSlice1',
      },
      {
        name: 'mockedName2',
        tag: 'mockedTag2',
        from: 4,
        to: 10,
        slice: 'mockedSlice2',
      }
    ],
    selectedVideo: 1,
    deleteVideo: jest.fn(),
    editClip: jest.fn(),
    setNotification: jest.fn(),
  };
  const renderedComponent = enzyme.shallow(
    <VideoList { ...mockedProps } />
  );
  it('Render matches snapshot', () => {
    expect(renderedComponent).toMatchSnapshot();
  });
  describe('Child events', () => {
    describe('Buttons', () => {
      afterEach(() => {
        jest.resetAllMocks();
      });
      it('play original Button calls handleClickRow', () => {
        const firstPlayButton = renderedComponent.find('Button').at(0);
        firstPlayButton.prop('onClick')();
        expect(mockedProps.handleClickRow).toHaveBeenCalledTimes(1);
      });
      it('play button in button group calls handleClickRow', () => {
        const playButton = renderedComponent.find('ButtonGroup').find('Button').at(0);
        playButton.prop('onClick')();
        expect(mockedProps.handleClickRow).toHaveBeenCalledTimes(1);
      });
      it('edit button in button group calls editClip', () => {
        const editButton = renderedComponent.find('ButtonGroup').find('Button').at(1);
        editButton.prop('onClick')();
        expect(mockedProps.editClip).toHaveBeenCalledTimes(1);
      });
      it('delete button in button group calls deleteVideo', () => {
        const deleteButton = renderedComponent.find('ButtonGroup').find('Button').at(2);
        deleteButton.prop('onClick')();
        expect(mockedProps.deleteVideo).toHaveBeenCalledTimes(1);
      });
    });
    it('CustomInput calls setState with given value', () => {
      const customInput = renderedComponent.find('CustomInput');
      customInput.prop('handleOnChange')('someValue');
      expect(renderedComponent.instance().state.filter).toEqual('someValue');
    });
  });
  describe('Methods', () => {
    let videoListInstance;
    beforeEach(() => {
      videoListInstance = renderedComponent.instance();
    });
    describe('matchesFilter', () => {
      it('returns true when filter state is null', () => {
        videoListInstance.setState({ filter: null });
        const result = videoListInstance.matchesFilter('mockedTag');
        expect(result).toBeTruthy();
      });
      it('returns true when filter state is empty string', () => {
        videoListInstance.setState({ filter: '' });
        const result = videoListInstance.matchesFilter('mockedTag');
        expect(result).toBeTruthy();
      });
      it('returns true when tag string includes filter state', () => {
        videoListInstance.setState({ filter: 'tag' });
        const result = videoListInstance.matchesFilter('mockedTag');
        expect(result).toBeTruthy();
      });
      it('returns false when tag string does not includes filter state', () => {
        videoListInstance.setState({ filter: 'nope' });
        const result = videoListInstance.matchesFilter('mockedTag');
        expect(result).toBeFalsy();
      });
    });
    describe('handleClickDownload', () => {
      it('calls setNotification with expected args', () => {
        const expectedArgs = {
          class: 'warning',
          title: downloadClicked.title,
          text: downloadClicked.content,
        };
        videoListInstance.handleClickDownload();
        expect(mockedProps.setNotification).toHaveBeenCalledTimes(1);
        expect(mockedProps.setNotification).toHaveBeenCalledWith(expectedArgs);
      });
    });
  });
});
