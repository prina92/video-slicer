import React from 'react';
import PropTypes from 'prop-types';
import { Well, Button } from 'react-bootstrap';

function Info({ ...props }) {
  return (
    <Well style={{ marginTop: 10 }}>
      <h4>Awesome!</h4>
      <p>
        You can check your clips in the list at your right (or below if your screen is not wider enough)
      </p>
      <p>
        You can also edit, delete, download or filter them by tag! <br/>
        (Note that filtering won't alter the playback order. It's just to help you find your clips faster!)
      </p>
      <p>
        Tip: You can go to your next clip or previous clip by pressing <strong>CTRL+ALT+N</strong> or <strong>CTRL+ALT+P</strong> respectively
      </p>
      <p>If you want to create more clips go to your <strong>original</strong> clip or just click the button below!</p>
      <div className="text-center">
        <Button bsStyle="primary" onClick={props.createClip}>
          Yes! More clips!
        </Button>
      </div>
    </Well>
  );
}

Info.propTypes = {
  createClip: PropTypes.func,
};

export default Info;
