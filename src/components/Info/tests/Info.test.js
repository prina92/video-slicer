import React from 'react';
import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Info from '../Info';

enzyme.configure({ adapter: new Adapter() });

describe('<Info />', () => {
  const mockedProps = {
    createClip: jest.fn(),
  };
  const renderedComponent = enzyme.shallow(
    <Info { ...mockedProps } />
  );
  it('Render matches snapshot', () => {
    expect(renderedComponent).toMatchSnapshot();
  });
});
