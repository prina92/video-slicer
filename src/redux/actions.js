// Every action returns a string and a payload, which redux will receive once the dispatch is done
import {
  SET_LOADING,
  SET_NOTIFICATION,
  SET_VIDEO_DURATION
} from "./constants";

export const setLoading = loading => {
  return { type: SET_LOADING, loading };
};
export const setNotification = notification => {
  return { type: SET_NOTIFICATION, notification };
};
export const setVideoDuration = duration => {
  return { type: SET_VIDEO_DURATION, duration };
};
