// File to store constants used both in actions and reducer

export const SET_LOADING = 'SET_LOADING';
export const SET_VIDEO_DURATION = 'SET_VIDEO_DURATION';
export const SET_NOTIFICATION = 'SET_NOTIFICATION';
