import { fromJS } from "immutable";
import {
  SET_NOTIFICATION,
  SET_LOADING,
  SET_VIDEO_DURATION,
} from "./constants";

// Initial state
export const initialState = fromJS({
  loading: false,
  notification: null,
  videoDuration: 0,
});

// Main reducer
function mainReducer(state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return state.set("loading", action.loading);
    case SET_NOTIFICATION:
      return state.set("notification", action.notification);
    case SET_VIDEO_DURATION:
      return state.set("videoDuration", action.duration);
    default:
      return state;
  }
}

export default mainReducer;
