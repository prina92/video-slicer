// Validates an array of fields with it's values and returns an array with errors. Length 0 means no errors found
export function durationInputValidation(fields) {
  const regExp = /^(([0-9]\d*):)([0-5]?\d)$/;
  let error = [];
  fields.forEach((field) => {
    if (!regExp.test(field.value) || field.value.split(':')[1].length !== 2) {
      error.push(field.name);
    }
  });
  return error;
}

export function validateClipName(clipName) {
  // validate is not null nor empty string
  if (!clipName || clipName === '') {
    return 'NULL_OR_EMPTY_ERROR';
  }
  // validate it doesn't contain a slash
  if (clipName.includes('/')) {
    return 'SLASH_ERROR';
  }
  return false;
}
