// Converts an integer into a time formatted string
export function toTimeFormat(number) {
  const sec_num = number.toFixed(0);
  let minutes = Math.floor(sec_num / 60);
  let seconds = sec_num - (minutes * 60);

  if (minutes < 10) {minutes = `0${minutes}`;}
  if (seconds < 10) {seconds = `0${seconds}`;}
  return `${minutes}:${seconds}`;
}

// Converts a time formatted string into an integer
export function timeStringToSeconds(timeString) {
  const arrayString = timeString.split(':'); // split it at the colons
  // minutes are worth 60 seconds.

  // if array has length 2 that mins it only contains minutes and seconds
  return (+arrayString[0]) * 60 + (+arrayString[1]);
}
