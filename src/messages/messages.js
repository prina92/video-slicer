// File for hardcoded messages used in notifications or modals

export const deleteModalContent = {
  title: 'Are you sure you want to delete this clip?',
  content: 'This operation cannot be undone!',
};

export const rangeErrorFormat = {
  title: 'Range error',
  content: 'It seems there\'s an error in your input. Please make sure your selected range follows the format MM:SS',
};

export const rangeErrorBounds = {
  title: 'Range error',
  content: 'It seems there\'s an error in your input. Please make sure your start time is lower than your end time.'
};

export const createClipError = {
  title: 'Creation error',
  content: 'There was an error while trying to create your clip.\n' +
    'Please check your input for possible errors in the range selected or the name of your clip.'
};

export const deleteClipError = {
  title: 'Error while deleting clip',
  content: 'There was an error while trying to delete your clip',
};

export const rangeErrors = {
  title: 'You have errors in your range',
  content: 'It seems you still have errors in the selected range. \n' +
    'Please check the format and make sure your Start time is lower than your End time',
};

export const clipNameErrorNullOrEmpty = {
  title: 'You have errors in your clip name',
  content: 'Please make sure you entered a name for your clip',
};

export const clipNameContainsSlash = {
  title: 'You have errors in your clip name',
  content: 'Please make sure your clip name is valid and doesn\'t contain any symbols (i.e "/")',
};

export const downloadClicked = {
  title: 'Your download will start shortly',
  content: 'This may take a while depending on the size of your clip. Please be patient!',
};

export const createClipSuccess = 'Clip created!';

export const updateClipSuccess = 'Clip updated!';

export const deleteClipSuccess = 'Clip deleted!';

export const videoUploadedSuccess = 'Video uploaded';

export const videoUploadError = 'There was an error uploading your file';
