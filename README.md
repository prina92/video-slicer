# React Video Slicer
This project allows you to upload a video and create clips that you can download.

Bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Getting started
React Video Slicer is a NodeJS + NPM based project that can be run on a computer
- Download  [NodeJS](https://nodejs.org/en/).
- Clone this project to your pc
- Open a terminal and navigate to the location where you just cloned the project
- Run `npm install` to install all the libraries required
- Run `npm start`
- A tab in your browser should automatically open in [http://localhost:3000](http://localhost:3000/ "http://localhost:3000/")

#### Test coverage:
You can run `npm test` to test the components and see the coverage.
I did my best to try and cover the majority of the components, however due to the time available I wasn't able to finish it completely (it's in my TODO list)

#### Notes
This project includes a little Node server that runs automatically along the client after you run `npm start`
The porpouse of this server is to process the video files, trim clips out of the uploaded video and return them as a download.
Due to it's small size, and for the sake of simplicity in this challenge I included the server along with the client, and they even share the package.json file.
This is **not** the way I would do it in a 'real life' project. Usually I'd have the server in a separated project, properly configured with security and connected to our client.

I have many ideas on how to improve this project to be a viable product, but time is not on my side to implement them and it's not the idea of this code challenge, isn't it? :)

I hope you like it!
